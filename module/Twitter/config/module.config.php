<?php

return [
    'controllers' => [
        'invokables' => [
            'Twitter\Controller\Tweet' => 'Twitter\Controller\TweetController',
            'Twitter\Controller\User' => 'Twitter\Controller\UserController',
            'Twitter\Controller\Site' => 'Twitter\Controller\SiteController',
        ],
    ],
    'router' => [
        'routes' => [
            'tweet' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/tweet[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => 'Twitter\Controller\Tweet',
                        'action' => 'index',
                    ],
                ],
            ],
            'user' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/user[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => 'Twitter\Controller\User',
                        'action' => 'index',
                    ],
                ],
            ],
            'site' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/site[/:action][/:id]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => 'Twitter\Controller\Site',
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'twitter' => __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];