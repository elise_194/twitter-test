<?php

namespace Twitter\Controller;

use Twitter\Service\TweetService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class TweetController.
 *
 * @package Twitter\Controller
 */
class TweetController extends AbstractActionController
{
    /**
     * @return array|JsonModel
     */
    public function indexAction()
    {
        $service = new TweetService($this->getServiceLocator(), 'Twitter\Model\TweetTable');
        return new JsonModel($service->get());
    }

    /**
     * @return JsonModel
     */
    public function addAction()
    {
        $service = new TweetService($this->getServiceLocator(), 'Twitter\Model\TweetTable');
        return new JsonModel($service->add($this->getRequest()));
    }

    /**
     * @return JsonModel
     */
    public function editAction()
    {
        $service = new TweetService($this->getServiceLocator(), 'Twitter\Model\TweetTable');
        return new JsonModel($service->edit($this->getRequest()));
    }

    /**
     * @return JsonModel
     */
    public function deleteAction()
    {
        $service = new TweetService($this->getServiceLocator(), 'Twitter\Model\TweetTable');
        return new JsonModel($service->delete($this->getRequest()));
    }
}
