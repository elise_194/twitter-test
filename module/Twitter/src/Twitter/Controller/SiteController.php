<?php

namespace Twitter\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class SiteController.
 *
 * @package Twitter\Controller
 */
class SiteController extends AbstractActionController
{
    /**
     * @return array|ViewModel
     */
    public function indexAction()
    {
        return $this->getViewModel();
    }

    /**
     * @return ViewModel
     */
    public function userAction()
    {
        return $this->getViewModel();
    }

    /**
     * @return ViewModel
     */
    protected function getViewModel()
    {
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }
}
