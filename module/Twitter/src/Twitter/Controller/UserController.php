<?php

namespace Twitter\Controller;

use Twitter\Service\UserService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

/**
 * Class TweetController.
 *
 * @package Twitter\Controller
 */
class UserController extends AbstractActionController
{
    /**
     * @return array|JsonModel
     */
    public function indexAction()
    {
        $service = new UserService($this->getServiceLocator(), 'Twitter\Model\UserTable');
        return new JsonModel($service->get());
    }

    /**
     * @return JsonModel
     */
    public function addAction()
    {
        $service = new UserService($this->getServiceLocator(), 'Twitter\Model\UserTable');
        return new JsonModel($service->add($this->getRequest()));
    }

    /**
     * @return JsonModel
     */
    public function editAction()
    {
        $service = new UserService($this->getServiceLocator(), 'Twitter\Model\UserTable');
        return new JsonModel($service->edit($this->getRequest()));
    }

    /**
     * @return JsonModel
     */
    public function deleteAction()
    {
        $service = new UserService($this->getServiceLocator(), 'Twitter\Model\UserTable');
        return new JsonModel($service->delete($this->getRequest()));
    }
}
