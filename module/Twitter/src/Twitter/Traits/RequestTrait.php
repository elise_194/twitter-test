<?php

namespace Twitter\Traits;

use Zend\Json\Json;
use Zend\Stdlib\RequestInterface;

/**
 * Trait ServiceTrait.
 *
 * @package Twitter\Traits
 */
trait RequestTrait
{
    /**
     * @param RequestInterface $request
     * @return mixed
     */
    protected function parseRequest(RequestInterface $request)
    {
        $body = $request->getContent();
        return Json::decode($body, Json::TYPE_ARRAY);
    }
}
