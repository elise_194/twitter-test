<?php

namespace Twitter\Service;

use Twitter\Model\Tweet;
use Twitter\Model\TweetTable;
use Zend\Stdlib\RequestInterface;
use Exception;

/**
 * Class TweetService
 * @package Twitter\Service
 */
class TweetService extends BaseService
{
    /**
     * @param RequestInterface $request
     * @return array
     */
    public function add(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var TweetTable $table */
        $table = $this->getTable();

        try {
            if ($request->isPost()) {
                $tweet = new Tweet();
                $tweet->exchangeArray($this->parseRequest($request));
                $table->saveTweet($tweet);
            }
        } catch (Exception $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }

    /**
     * @param RequestInterface $request
     * @return array
     */
    public function edit(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var TweetTable $table */
        $table = $this->getTable();

        try {
            $data = $this->parseRequest($request);
            $tweet = $table->getTweet($data['id']);
            $tweet->user_id = $data['user_id'];
            $tweet->title = $data['title'];
            $tweet->content = $data['content'];

            $table->saveTweet($tweet);
        } catch (Exception $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }

    /**
     * @param RequestInterface $request
     * @return array
     */
    public function delete(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var TweetTable $table */
        $table = $this->getTable();

        try {
            $data = $this->parseRequest($request);
            $table->deleteTweet($data['id']);
        } catch (\Throwable $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }
}
