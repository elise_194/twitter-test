<?php

namespace Twitter\Service;

use Twitter\Model\User;
use Twitter\Model\UserTable;
use Zend\Stdlib\RequestInterface;
use Exception;

/**
 * Class UserService.
 *
 * @package Twitter\Service
 */
class UserService extends BaseService
{
    /**
     * @param RequestInterface $request
     * @return array
     */
    public function add(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var UserTable $table */
        $table = $this->getTable();

        try {
            if ($request->isPost()) {
                $user = new User();
                $user->exchangeArray($this->parseRequest($request));
                $table->saveUser($user);
            }
        } catch (Exception $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }

    /**
     * @param RequestInterface $request
     * @return array
     */
    public function edit(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var UserTable $table */
        $table = $this->getTable();

        try {
            $data = $this->parseRequest($request);
            $user = $table->getUser($data['id']);
            $user->email = $data['email'];
            $user->login = $data['login'];
            $user->name = $data['name'];

            $table->saveUser($user);
        } catch (Exception $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }

    /**
     * @param RequestInterface $request
     * @return array
     */
    public function delete(RequestInterface $request)
    {
        $result = true;
        $error = null;

        /** @var UserTable $table */
        $table = $this->getTable();

        try {
            $data = $this->parseRequest($request);
            $table->deleteUser($data['id']);
        } catch (\Throwable $exception) {
            $result = false;
            $error = $exception->getMessage();
        }

        return [
            'result' => $result,
            'error' => $error,
        ];
    }
}
