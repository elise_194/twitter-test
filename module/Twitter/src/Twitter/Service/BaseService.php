<?php

namespace Twitter\Service;

use Twitter\Traits\RequestTrait;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class BaseService.
 *
 * @package Twitter\Service
 */
abstract class BaseService
{
    use RequestTrait;

    public $serviceLocator;
    public $table;

    /**
     * ServiceTrait constructor.
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @param string $tableClass
     */
    public function __construct(ServiceLocatorInterface $serviceLocator, string $tableClass)
    {
        $this->serviceLocator = $serviceLocator;
        $this->setTable($tableClass);
    }

    /**
     * @param $tableClass
     */
    public function setTable($tableClass)
    {
        if (!$this->table) {
            $this->table = $this->serviceLocator->get($tableClass);
        }
    }

    /**
     * @return mixed
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->table
            ->fetchAll()
            ->toArray();
    }
}
