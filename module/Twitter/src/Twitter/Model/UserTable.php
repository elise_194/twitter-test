<?php

namespace Twitter\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Exception;

/**
 * Class UserTable.
 *
 * @package Twitter\Model
 */
class UserTable
{
    /** @var TableGateway $tableGateway */
    protected $tableGateway;

    /**
     * UserTable constructor.
     *
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return ResultSet
     */
    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    /**
     * @param int $id
     * @return array|User
     * @throws Exception
     */
    public function getUser($id)
    {
        $rowSet = $this->tableGateway->select(['id' => (int) $id]);
        $row = $rowSet->current();
        if (!$row) {
            throw new Exception("Could not find row $id");
        }

        return $row;
    }

    /**
     * @param User $user
     * @throws Exception
     */
    public function saveUser(User $user)
    {
        $data = [
            'email' => $user->email,
            'login'  => $user->login,
            'name'  => $user->name,
        ];

        if ($user->id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getUser($user->id)) {
                $this->tableGateway->update($data, ['id' => $user->id]);
            } else {
                throw new Exception('User id does not exist');
            }
        }
    }

    /**
     * @param int $id
     */
    public function deleteUser($id)
    {
        $this->tableGateway->delete(['id' => $id]);
    }
}
