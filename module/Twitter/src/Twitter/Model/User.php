<?php

namespace Twitter\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;
use Exception;

/**
 * Class User.
 *
 * @package Twitter\Model
 *
 * @property integer $id
 * @property string $email
 * @property string $login
 * @property string $name
 */
class User
{
    public $id;
    public $email;
    public $login;
    public $name;

    protected $inputFilter;

    /**
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->login = (!empty($data['login'])) ? $data['login'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
    }

    /**
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    /**
     * @param InputFilterInterface $inputFilter
     * @throws Exception
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $inputFilter->add([
                'name' => 'id',
                'required' => true,
                'filters' => [
                    ['name' => 'Int'],
                ],
            ]);

            $fields = ['email', 'login', 'name'];
            foreach ($fields as $field) {
                $inputFilter->add([
                    'name' => $field,
                    'required' => true,
                    'filters' => [
                        ['name' => 'StripTags'],
                        ['name' => 'StringTrim'],
                    ],
                    'validators' => [
                        [
                            'name' => 'StringLength',
                            'options' => [
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ],
                        ],
                    ],
                ]);
            }

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
}
