<?php

namespace Twitter\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Exception;

/**
 * Class TweetTable.
 *
 * @package Twitter\Model
 */
class TweetTable
{
    /** @var TableGateway $tableGateway */
    protected $tableGateway;

    /**
     * UserTable constructor.
     *
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @return ResultSet
     */
    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    /**
     * @param int $id
     * @return array|Tweet
     * @throws Exception
     */
    public function getTweet($id)
    {
        $rowSet = $this->tableGateway->select(['id' => (int) $id]);
        $row = $rowSet->current();
        if (!$row) {
            throw new Exception("Could not find row $id");
        }

        return $row;
    }

    /**
     * @param Tweet|object $tweet
     * @throws Exception
     */
    public function saveTweet(Tweet $tweet)
    {
        $data = [
            'user_id' => $tweet->user_id,
            'title' => $tweet->title,
            'content' => $tweet->content,
        ];

        if ($tweet->id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getTweet($tweet->id)) {
                $this->tableGateway->update($data, ['id' => $tweet->id]);
            } else {
                throw new Exception('Tweet id does not exist');
            }
        }
    }

    /**
     * @param int $id
     */
    public function deleteTweet($id)
    {
        $this->tableGateway->delete(['id' => $id]);
    }
}
