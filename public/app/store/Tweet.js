Ext.define('Twitter.store.Tweet',
  {
    extend: 'Ext.data.Store',
    model: 'Twitter.model.Tweet',
    autoLoad: false,
    storeId: 'Tweet'
  });
