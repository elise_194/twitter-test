Ext.define('Twitter.view.TweetGrid',
  {
    extend: 'Ext.grid.Panel',
    alias: 'widget.TweetGrid',
    id: 'tweetGrid',
    config: {},
    constructor: function (config) {
      this.initConfig(config);
      return this.callParent(arguments);
    },
    width: '100%',
    height: 400,
    selType: 'checkboxmodel',
    title: 'Tweet Information',
    selModel:
      {
        mode: 'MULTI'
      },
    viewConfig:
      {
        stripeRows: true
      },
    initComponent: function () {
      Ext.apply(this,
        {
          store: 'Twitter.store.Tweet',

          plugins: [Ext.create('Ext.grid.plugin.RowEditing',
            {
              clicksToEdit: 2
              //if you have checkbox in first row then take clicksToEdit=2 otherwise it will go on edit mode
            })],

          columns: [
            {
              text: "Id",
              dataIndex: 'id',
              hidden: false,
              width: 35
            },
            {
              text: "Заголовок",
              flex: 1,
              dataIndex: 'title',
              editor:
                {
                  allowBlank: false
                }
            },
            {
              header: "Автор",
              flex: 1,
              dataIndex: 'user_id',
              editor: new Ext.form.field.ComboBox({
                typeAhead: true,
                triggerAction: 'all',
                store: [
                  [1, 'elise@mail.ru'],
                  [2, 'mike@mail.ru'],
                  [3, 'leonid@mail.ru'],
                  [4, 'oleg@mail.ru'],
                  [5, 'alexios@mail.ru'],
                ]
              })
            },
            {
              text: "Контент",
              flex: 1,
              dataIndex: 'content',
              editor:
                {
                  allowBlank: true
                }
            }],
          dockedItems: [{
            xtype: 'toolbar',
            dock: 'bottom',
            ui: 'footer',
            layout:
              {
                pack: 'center'
              },
            defaults:
              {
                minWidth: 80
              },
            items: [
              {
                text: 'Create',
                itemId: 'btnCreate'
              },
              {
                text: 'Load Data',
                itemId: 'btnLoad'
              },
              {
                text: 'Save',
                itemId: 'btnSave'
              },
              {
                text: 'Delete',
                itemId: 'btnDelete'
              }]
          }]
        });

      this.callParent(arguments);
    }
  });
