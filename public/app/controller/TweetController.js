Ext.define('Twitter.controller.TweetController',
  {
    extend: 'Ext.app.Controller',
    models: ['Twitter.model.Tweet'],
    stores: ['Twitter.store.Tweet'],
    views: ['Twitter.view.TweetGrid'],
    refs: [{
      ref: 'tweetGrid',
      selector: 'viewport TweetGrid'
    }],

    init: function () {

      this.control({

        'viewport > TweetGrid button[itemId=btnCreate]':
          {
            click: this.onCreateClick
          },
        'viewport > TweetGrid button[itemId=btnLoad]':
          {
            click: this.onLoadClick
          },
        'viewport > TweetGrid button[itemId=btnSave]':
          {
            click: this.onSaveClick
          },
        'viewport > TweetGrid button[itemId=btnDelete]':
          {
            click: this.onDeleteClick
          }
      });

      var tweetStore = Ext.getStore('Twitter.store.Tweet');
      tweetStore.load();
    },

    onCreateClick: function () {

      var tweetGrid = this.getTweetGrid();
      var tweetStore = tweetGrid.getStore();

      var tweetModel = Ext.create('Twitter.model.Tweet');
      tweetModel.set("name", "Введите название книги");
      tweetModel.set("user_id", 0);
      tweetModel.set("title", 0);
      tweetModel.set("content", 0);
      tweetModel.set("created_at", 0);

      tweetStore.add(tweetModel);

      tweetGrid.editingPlugin.startEdit(tweetModel, 3);

    },

    onSaveClick: function () {
      var tweetGrid = this.getTweetGrid();
      var tweetStore = tweetGrid.getStore();

      //fires create, update and delete request when calling sync and commit changes in the store when autoSync=false
      tweetStore.sync({
        success: function (batch, eOpts) {
          Ext.Msg.alert('Status', 'Changes saved successfully.');
        },
        failure: function (batch, eOpts) {
          Ext.Msg.alert('Status', 'Request failed.');
        }
      });
    },
    onLoadClick: function () {
      var tweetStore = Ext.getStore('Twitter.store.Tweet');
      tweetStore.load();
    },

    onDeleteClick: function () {

      var tweetGrid = this.getTweetGrid();
      var tweetStore = tweetGrid.getStore();

      //delete selected rows if selModel is checkboxmodel
      var selectedRows = tweetGrid.getSelectionModel().getSelection();

      if (selectedRows.length)
        tweetStore.remove(selectedRows);
      else
        Ext.Msg.alert('Status', 'Please select at least one record to delete!');
    }
  });
