Ext.define('Twitter.model.Tweet',
  {
    extend : 'Ext.data.Model',
    idProperty:'Id',
    fields: [
      { name: 'id', type: 'int', defaultValue: 0 },
      { name: 'title', type: 'string' },
      { name: 'user_id', type: 'integer' },
      { name: 'content', type: 'string' },
      { name: 'created-at', type: 'string' }
    ],
    validations : [{
      type : 'presence',
      field : 'name'
    } ],
    proxy :
      {
        type : 'ajax',
        reader :
          {
            root:'data',
            type : 'json'
          },
        api :
          {
            read : '/tweet',
            create : '/tweet/add',
            update : '/tweet/edit',
            destroy : '/tweet/delete'
          },
        actionMethods :
          {
            destroy : 'POST',
            read : 'GET',
            create : 'POST',
            update : 'POST'
          }
      }
  });
