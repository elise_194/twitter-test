Ext.application({
  requires: ['Ext.container.Viewport' ],
  name : 'Twitter',
  controllers : ['TweetController'],

  launch: function () {

    Ext.create('Ext.container.Viewport',
      {
        layout : 'anchor',
        items : [{
          xtype: 'TweetGrid'
        }]
      });
  }
});
