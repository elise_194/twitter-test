Ext.define('Twitter.controller.UserController',
  {
    extend: 'Ext.app.Controller',
    models: ['Twitter.model.User'],
    stores: ['Twitter.store.User'],
    views: ['Twitter.view.UserGrid'],
    refs: [{
      ref: 'userGrid',
      selector: 'viewport UserGrid'
    }],

    init: function () {

      this.control({

        'viewport > UserGrid button[itemId=btnCreate]':
          {
            click: this.onCreateClick
          },
        'viewport > UserGrid button[itemId=btnLoad]':
          {
            click: this.onLoadClick
          },
        'viewport > UserGrid button[itemId=btnSave]':
          {
            click: this.onSaveClick
          },
        'viewport > UserGrid button[itemId=btnDelete]':
          {
            click: this.onDeleteClick
          }
      });

      var userStore = Ext.getStore('Twitter.store.User');
      userStore.load();
    },

    onCreateClick: function () {

      var userGrid = this.getUserGrid();
      var userStore = userGrid.getStore();

      var userModel = Ext.create('Twitter.model.User');
      userModel.set("email", "E-mail");
      userModel.set("login", "Login");
      userModel.set("name", "Имя");

      userStore.add(userModel);

      userGrid.editingPlugin.startEdit(userModel, 3);
    },

    onSaveClick: function () {
      var userGrid = this.getUserGrid();
      var userStore = userGrid.getStore();

      //fires create, update and delete request when calling sync and commit changes in the store when autoSync=false
      userStore.sync({
        success: function (batch, eOpts) {
          Ext.Msg.alert('Status', 'Changes saved successfully.');
        },
        failure: function (batch, eOpts) {
          Ext.Msg.alert('Status', 'Request failed.');
        }
      });
    },
    onLoadClick: function () {
      var userStore = Ext.getStore('Twitter.store.User');
      userStore.load();
    },

    onDeleteClick: function () {

      var userGrid = this.getUserGrid();
      var userStore = userGrid.getStore();

      //delete selected rows if selModel is checkboxmodel
      var selectedRows = userGrid.getSelectionModel().getSelection();

      if (selectedRows.length)
        userStore.remove(selectedRows);
      else
        Ext.Msg.alert('Status', 'Please select at least one record to delete!');
    }
  });
