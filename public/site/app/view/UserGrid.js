Ext.define('Twitter.view.UserGrid',
  {
    extend : 'Ext.grid.Panel',
    alias : 'widget.UserGrid',
    id : 'userGrid',
    config : {},
    constructor : function(config){
      this.initConfig(config);
      return this.callParent(arguments);
    },
    width : '100%',
    height : 400,
    selType : 'checkboxmodel',
    title : 'User information',
    selModel :
      {
        mode : 'MULTI'
      },
    viewConfig :
      {
        stripeRows : true
      },
    initComponent : function(){
      Ext.apply(this,
        {
          store : 'Twitter.store.User',

          plugins : [Ext.create('Ext.grid.plugin.RowEditing',
            {
              clicksToEdit : 2
              //if you have checkbox in first row then take clicksToEdit=2 otherwise it will go on edit mode
            })],

          columns : [{
            text : "Id",
            dataIndex : 'id',
            hidden : false,
            width : 35
          },
            {
              text : "E-mail",
              flex : 1,
              dataIndex : 'email',
              editor :
                {
                  allowBlank : false
                }
            },
            {
              text : "Login",
              flex : 1,
              dataIndex : 'login',
              editor :
                {
                  allowBlank : true
                }
            },
            {
              text : "Имя",
              flex : 1,
              dataIndex : 'name',
              editor :
                {
                  allowBlank : true
                }
            }],
          dockedItems : [{
            xtype : 'toolbar',
            dock : 'bottom',
            ui : 'footer',
            layout :
              {
                pack : 'center'
              },
            defaults :
              {
                minWidth : 80
              },
            items : [
              {
                text : 'Create',
                itemId : 'btnCreate'
              },
              {
                text : 'Load Data',
                itemId : 'btnLoad'
              },
              {
                text : 'Save',
                itemId : 'btnSave'
              },
              {
                text : 'Delete',
                itemId : 'btnDelete'
              }]
          }]
        });

      this.callParent(arguments);
    }
  });
