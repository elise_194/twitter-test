Ext.define('Twitter.store.User',
  {
    extend: 'Ext.data.Store',
    model: 'Twitter.model.User',
    autoLoad: false,
    storeId: 'User'
  });
