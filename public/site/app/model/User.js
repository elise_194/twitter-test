Ext.define('Twitter.model.User',
  {
    extend : 'Ext.data.Model',
    idProperty:'id',
    fields: [
      { name: 'id', type: 'int', defaultValue: 0 },
      { name: 'email', type: 'string' },
      { name: 'login', type: 'string' },
      { name: 'name', type: 'string' }
    ],
    validations : [{
      type : 'presence',
      field : 'email'
    } ],
    proxy :
      {
        type : 'ajax',
        reader :
          {
            root:'data',
            type : 'json'
          },
        api :
          {
            read : '/user',
            create : '/user/add',
            update : '/user/edit',
            destroy : '/user/delete'
          },
        actionMethods :
          {
            destroy : 'POST',
            read : 'GET',
            create : 'POST',
            update : 'POST'
          }
      }
  });
