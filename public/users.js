Ext.application({
  requires: ['Ext.container.Viewport' ],
  name : 'Twitter',
  controllers : ['UserController'],
  autoCreateViewport: false,
  launch: function () {

    Ext.create('Ext.container.Viewport',
      {
        layout : 'anchor',
        items : [{
          xtype: 'UserGrid'
        }]
      });
  }
});
