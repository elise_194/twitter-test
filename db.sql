create table "user"
(
    id serial not null
        constraint user_pkey
        primary key,
    email varchar(255) not null,
    login varchar(255) not null,
    name varchar(255) not null
);

create table "tweet"
(
    id serial not null
        constraint tweet_pkey
            primary key,
    user_id integer not null references "user"(id),
    title varchar(255) not null,
    content varchar(280) not null,
    created_at timestamp(0) with time zone default now() not null
);

insert into "user"
    (email, login, name)
VALUES ('elise@mail.ru', 'Elise', 'Elise'),
       ('mike@mail.ru', 'mike', 'Michail'),
       ('leonid@mail.ru', 'leonid', 'Leonid'),
       ('oleg@mail.ru', 'oleg', 'Oleg'),
       ('alexios@mail.ru', 'alex', 'Alexeios');
